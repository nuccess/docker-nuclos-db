#
# Nuclos DB based on Postgres ${BASE_IMAGE_TAG} Dockerfile
#
# https://bitbucket.org/nuccess/docker-nuclos-db
ARG BASE_IMAGE_TAG=missing
FROM library/postgres:${BASE_IMAGE_TAG}
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y sudo locales vim

RUN echo "postgres ALL=(ALL) NOPASSWD: /usr/bin/localedef" >> /etc/sudoers && \
  echo "postgres ALL=(ALL) NOPASSWD: /usr/sbin/update-locale" >> /etc/sudoers && \
  echo "postgres ALL=(ALL) NOPASSWD: /usr/bin/chmod" >> /etc/sudoers

ENV PGDATA=/var/lib/postgresql/nuclos \
  POSTGRES_DB=nuclosdb \
  POSTGRES_USER=nuclos \
  LOCALE=de_DE.UTF-8 \
  TZ=Europe/Berlin \
  TOTAL_RAM_GB=1.0 \
  MAX_RAM_PERCENTAGE=75 \
  MAX_CONNECTIONS=50

ADD nuclos-db-init.sh /usr/local/bin
ADD nuclos-db-config.sh /usr/local/bin
ADD nuclos-db-run.sh /usr/local/bin
ADD execute-db-cmds.sh /usr/local/bin
RUN chmod +x /usr/local/bin/*.sh

ENV EXECUTE_CMD_DIR=/var/nuclos-db
ENV EXECUTE_CMD_BACKUPS_DIR=$EXECUTE_CMD_DIR/backups
ENV EXECUTE_CMD_LOGS_DIR=$EXECUTE_CMD_DIR/logs
# Optimize DB for Nuclos
ADD postgresql.conf /var/lib/postgresql/nuclosdb-postgresql.conf-template
RUN chown 999 /var/lib/postgresql/nuclosdb-postgresql.conf-template && \
    chgrp 999 /var/lib/postgresql/nuclosdb-postgresql.conf-template

RUN mkdir /var/nuclos-db/ && \
    chown 999 /var/nuclos-db/ && \
    chgrp 1000 /var/nuclos-db/ && \
    chmod 770 /var/nuclos-db/

# Add VOLUME to allow DDL, backups or restore
VOLUME ["/var/nuclos-db/", "/opt/nuclos/secrets"]

USER postgres

STOPSIGNAL SIGTERM

CMD ["nuclos-db-run.sh"]
