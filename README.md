# Nuclos-DB

This repository contains the [PostgreSQL](https://www.postgresql.org/) database for [Nuclos container](https://hub.docker.com/r/nuccess/nuclos-server/) and is build by an automated pipeline and published to the public [Docker Hub Registry](https://registry.hub.docker.com/).

> ⚠️ **Important change**: As of 17.0, the `DB_PASSWORD` parameter is no longer available. For security reasons, it is replaced by the file `db_password` in the volume `/opt/nuclos/secrets` . However, the default password has not changed, and an adaption will only be necessary if you have changed it.

## Base Docker Image

* [postgres/BASE_IMAGE_TAG_PLACEHOLDER](https://hub.docker.com/_/postgres/)

## Docker Tags

Visit [Tags](https://hub.docker.com/r/nuccess/nuclos-db/tags/)

## Source Code

Visit [BitBucket](https://bitbucket.org/nuccess/docker-nuclos-db/src/master/)

## Installation

1. Install [Docker](https://www.docker.com/).

2. Download automated build from public [Docker Hub Registry](https://registry.hub.docker.com/): `docker pull nuccess/nuclos-db`

   (alternatively, you can build an image from Dockerfile: `docker build --build-arg BASE_IMAGE_TAG=BASE_IMAGE_TAG_PLACEHOLDER --tag my-nuclos-db https://bitbucket.org/nuccess/docker-nuclos-db.git`)

---

# Usage

    docker run --name nuclos-db-BASE_IMAGE_TAG_PLACEHOLDER -e TZ=Europe/Berlin -e LOCALE=de_DE.UTF-8 -p 5496:5432 -d nuccess/nuclos-db:BASE_IMAGE_TAG_PLACEHOLDER

After a reboot, you can simply restart the container with: `docker start nuclos-db-BASE_IMAGE_TAG_PLACEHOLDER`

## Data Volume `/var/nuclos-db`
This directory acts as an interface for a Nuclos application server container. New files in this directory are automatically processed by the server container. You can also use this function to import or export a database dump (created with pg_dump). No need to install a PostgreSQL client on your host system or to connect to the container directly.

To access this volume from your host system, just mount it to a local directory using following command. E.g. `-v /Users/Maik/Nuclos/Docker/vol-nuclos-db-BASE_IMAGE_TAG_PLACEHOLDER:/var/nuclos-db` 

* ### Import a dump (pg_dump format)
1. Copy the dump to the subdirectory `/var/nuclos-db/backups`. The name of the dump file must ensure following naming convention: `schema#tag.backup` (**schema**: The schema is then automatically created. If it already exists, it will be dropped before creating it again. **tag**: Name or timestamp, only to identify a dump). Example: *myerp#2017-05-01.backup*
2. To instruct the container to import the dump, create a simple file `schema#tag.imp` in */var/nuclos-db*, which is then automatically interpreted as a command. Example: `echo "FileContentDoesNotMatter" > /Users/Maik/Nuclos/Docker/vol-nuclos-db-BASE_IMAGE_TAG_PLACEHOLDER/myerp#2017-05-01.imp` (the mounted directory as explained in section "Data Volume").
3. Once the import is completed, the recently created "command file" e.g. *myerp#2017-05-01.imp* will be deleted automatically.

* ### Export a schema (pg_dump format)
1. Create a simple file `schema#tag.exp` in */var/nuclos-db*. Example: `echo "FileContentDoesNotMatter" > /Users/Maik/Nuclos/Docker/vol-nuclos-db-BASE_IMAGE_TAG_PLACEHOLDER/myerp#2017-05-15.exp` (the mounted directory as explained in section "Data Volume").
2. Once the export is completed, this "command file" is deleted automatically. The exported dump will be stored in the subdirectory named `backups`. E.g. `var/nuclos-db/backups` 

* ### Execute SQL
1. Save your SQL commands in one single file. Use the file name extension **.sql** and copy it to *var/nuclos-db*

---

## Environment Variables

* ### TZ
Set the timezone. Default is Europe/Berlin. `-e TZ=Europe/Berlin`

* ### LOCALE
Set the locale. Default is de_DE.UTF-8. `-e LOCALE=de_DE.UTF-8`

* ### POSTGRES_USER
The default PostgreSQL user is `nuclos` with password `password`. You can change the default password with the file `db_password` in the volume `/opt/nuclos/secrets` .

* ### TOTAL_RAM_GB
Set total usable RAM for the Container. If the container is started with a limit (--memory), this has priority. Default is 1 GB. `-e TOTAL_RAM_GB=4`

* ### MAX_RAM_PERCENTAGE
Dynamically controls the total usable RAM for the PostgreSQL process. Default is 75%. `-e MAX_RAM_PERCENTAGE=85`

* ### MAX_CONNECTIONS
PostgreSQL database connection limit. Default is 50. We recommend 50 connections per active Nuclos application server. `-e MAX_CONNECTIONS=50`


## Data Volumes

* ### `/opt/nuclos/secrets`
Directory of all secrets if you want to use something other than the default. 
#### db_password
Contains the password to be set for the `POSTGRES_USER`
