#!/bin/bash

# Create dirs
mkdir -p $EXECUTE_CMD_BACKUPS_DIR
mkdir -p $EXECUTE_CMD_LOGS_DIR

if [ "$NO_NEW_PRIVILEGES" -eq 1 ]; then
  chmod a+rw $EXECUTE_CMD_DIR
  chmod a+rw $EXECUTE_CMD_BACKUPS_DIR
  chmod a+rw $EXECUTE_CMD_LOGS_DIR
else
  sudo chmod a+rw $EXECUTE_CMD_DIR
  sudo chmod a+rw $EXECUTE_CMD_BACKUPS_DIR
  sudo chmod a+rw $EXECUTE_CMD_LOGS_DIR
fi

#  Optimize for Nuclos (Mixed type app + SSD)
rm -f $PGDATA/postgresql.conf
cp /var/lib/postgresql/nuclosdb-postgresql.conf-template $PGDATA/postgresql.conf

TOTAL_RAM_GB=${TOTAL_RAM_GB:-1}
# Memory limit in bytes (64 GB = 64 * 1024 * 1024 * 1024 bytes)
LIMIT_64GB=$((64 * 1024 * 1024 * 1024))
# Read the memory limit value from the cgroup v2 file
MEM_LIMIT_BYTES=$(cat /sys/fs/cgroup/memory.max)
# Decision: Use the cgroup memory limit or the custom value
if [ "${MEM_LIMIT_BYTES:-${LIMIT_64GB}}" -lt "$LIMIT_64GB" ]; then
  # If memory is less than 64 GB, use the cgroup value
  echo "db-config: Using cgroup memory limit: $((MEM_LIMIT_BYTES / 1024 / 1024)) MB"
  RAM=$((MEM_LIMIT_BYTES / 1024 / 1024))  # Convert to MB
else
  # Otherwise, use the custom TOTAL_RAM_GB value
  echo "db-config: Using custom TOTAL_RAM_GB: ${TOTAL_RAM_GB} GB"
  RAM=$(printf "%.0f" "$(awk "BEGIN {print $TOTAL_RAM_GB * 1024}")")
fi

# Calculate the final RAM value based on the percentage
DB_MAX_RAM_PERCENTAGE=${DB_MAX_RAM_PERCENTAGE:-${MAX_RAM_PERCENTAGE:-75}}
RAM=$(printf "%.0f" "$(awk "BEGIN {print $RAM * $DB_MAX_RAM_PERCENTAGE / 100}")")
echo "db-config: Total DB RAM after ${DB_MAX_RAM_PERCENTAGE}% limit: ${RAM} MB"

SHARED_BUFFERS=$((RAM/4))
EFFECTIVE_CACHE_SIZE=$((RAM-SHARED_BUFFERS))
MAINTENANCE_WORK_MEM=$((RAM/16))
WORK_MEM=$((RAM*1024/16/${DB_MAX_CONNECTIONS:-${MAX_CONNECTIONS:-100}}))
if (( RAM < 2048 )); then
  WAL_BUFFERS="$(printf "%.0f" "$(awk "BEGIN {print $SHARED_BUFFERS * 1024 / 32}")")kB"
else
  WAL_BUFFERS="16MB"
fi

sed -i 's:MAX_CONNECTIONS:'"${DB_MAX_CONNECTIONS:-${MAX_CONNECTIONS:-100}}"':g' $PGDATA/postgresql.conf
sed -i 's:SHARED_BUFFERS:'"$SHARED_BUFFERS"MB':g' $PGDATA/postgresql.conf
sed -i 's:EFFECTIVE_CACHE_SIZE:'"$EFFECTIVE_CACHE_SIZE"MB':g' $PGDATA/postgresql.conf
sed -i 's:MAINTENANCE_WORK_MEM:'"$MAINTENANCE_WORK_MEM"MB':g' $PGDATA/postgresql.conf
sed -i 's:WORK_MEM:'"$WORK_MEM"kB':g' $PGDATA/postgresql.conf
sed -i 's:WAL_BUFFERS:'"$WAL_BUFFERS"':g' $PGDATA/postgresql.conf
# setting locale & timezone
sed -i 's:LOCALE:'"$LOCALE"':g' $PGDATA/postgresql.conf
sed -i 's:TZ:'"$TZ"':g' $PGDATA/postgresql.conf
# allow access from outside the container
sed -i '1s/^/listen_addresses = \x27*\x27\n/' $PGDATA/postgresql.conf
