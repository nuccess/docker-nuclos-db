#!/bin/bash

# terminate container function
function _container_SIGTERM(){
  echo "Caught SIGTERM"
  $PGCTL -D $PGDATA stop
  exit 0
}
trap _container_SIGTERM SIGTERM

# 1 = yes
export NO_NEW_PRIVILEGES=$(grep "NoNewPrivs" /proc/self/status | awk '{print $2}')

# Set the locale
if [ -n "$LOCALE" ]; then
	GREP_LOCALE=$(grep "^$LOCALE " /usr/share/i18n/SUPPORTED)
	if [ -n "$GREP_LOCALE" ]; then
		LOCALE_ENCODING=${GREP_LOCALE##* }
		LOCALE_WITHOUT_ENCODING=${LOCALE%.*}
		export LANG=$LOCALE_WITHOUT_ENCODING.$LOCALE_ENCODING
    echo "Set locale to $LANG"
    if [ "$NO_NEW_PRIVILEGES" -eq 1 ]; then
      export LC_MESSAGES=POSIX
    else
      sudo localedef -i $LOCALE_WITHOUT_ENCODING -c -f $LOCALE_ENCODING /usr/lib/locale/$LANG
      sudo update-locale LANG=$LANG LC_MESSAGES=POSIX
    fi
	else
		echo "Locale \"$LOCALE\" is not supported. See list of available /usr/share/i18n/SUPPORTED:"
		cat /usr/share/i18n/SUPPORTED
	fi
fi

export DB_PASSWORD=$(tr -d '\r\n' < /opt/nuclos/secrets/db_password || echo "password")
# Set postgres password
export POSTGRES_PASSWORD=$DB_PASSWORD

if [ ! -d $PGDATA ] || [ -z "$(ls -A $PGDATA)" ]; then
  echo "init ($PGDATA)"
	nuclos-db-init.sh
	echo
fi

echo "configure ($PGDATA)"
nuclos-db-config.sh

# Start 'daemon' command executor
execute-db-cmds.sh &

echo "create $POSTGRES_DB and user $POSTGRES_USER if not already done"
export PGCTL=$(find /usr/lib -name pg_ctl | grep /bin/)
$PGCTL -D $PGDATA start >/dev/null 2>&1
createuser -s $POSTGRES_USER >/dev/null 2>&1
createdb -O $POSTGRES_USER $POSTGRES_DB >/dev/null 2>&1
psql -c "ALTER USER $POSTGRES_USER WITH PASSWORD '$DB_PASSWORD';" >/dev/null
echo "set timezone to $TZ"
psql -c "ALTER USER $POSTGRES_USER SET TIMEZONE='$TZ';" >/dev/null
$PGCTL -D $PGDATA stop >/dev/null
echo "start ($PGDATA)"
postgres -D $PGDATA &
wait
